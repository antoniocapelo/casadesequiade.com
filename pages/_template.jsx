import React from 'react';
import { Container } from 'react-responsive-grid';
import Header from '../shared/components/header/Header';
import Footer from '../shared/components/footer/Footer';
import { Link } from 'react-router';
import { prefixLink } from 'gatsby-helpers';
import '../css/markdown-styles';
import styles from '../shared/style/global.module.css';

import { rhythm } from '../utils/typography'

module.exports = React.createClass({
  propTypes () {
    return {
      children: React.PropTypes.any,
    }
  },
  render () {
    return (
      <div>
        <Header path={this.props.location.pathname} history={ this.props.history }/>
        <Container
            className={ styles.container }
              style={{
                maxWidth: 'none',
              }}
            >
          {this.props.children}
        </Container>
        <Footer />
      </div>
    )
  },
})
