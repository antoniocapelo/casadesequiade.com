import classNames from "classnames/bind";
import { config } from "config";
import React from "react";
import Helmet from "react-helmet";
import { translate } from "react-polyglot";
import seal from "../assets/images/seal.png";
import Contacts from "./contacts/Contacts";
import Details from "./details/Details";
// Components
import ImageGallery from "./image-gallery/ImageGallery";
// Styles
import styles from "./index.module.css";
import Location from "./location/Location";

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showContent: false,
    };
  }

  componentDidMount() {
    window.scroll(0, 0);
    setTimeout(() => {
      this.setState({ showContent: true });
      if (this.props.location.hash) {
        const element = document.querySelector(this.props.location.hash);

        if (element) {
          element.scrollIntoView({ behavior: "smooth" });
        }
      }
    }, 750);
  }

  render() {
    const { t } = this.props;
    const cx = classNames.bind(styles); // eslint-disable-line react/jsx-no-bind
    const className = cx("contentWrapper", { hidden: !this.state.showContent }); // => "abc def xyz"
    const imgClassname = cx("seal", { hidden: !this.state.showContent }); // => "abc def xyz"

    return (
      <div className={styles.homeWrapper}>
        <div className={styles.home}>
          <img src={seal} className={imgClassname} />
          <Helmet
            title={config.siteTitle}
            meta={[
              { name: "description", content: t("main.description") },
              { name: "keywords", content: t("main.keywords") },
            ]}
          />
          <h1 className={styles.homeTitle}>Casa de Sequiade</h1>
          <h3 className={styles.homeSubtitle}>~ Turismo Rural ~</h3>
          <div className={styles.separator} />
          <div className={className}>
            <div className={styles.copy}>
              <p>{t("home.intro")}</p>
              <Details />
            </div>
            <ImageGallery showLink />
            <Location />
            <Contacts />
          </div>
        </div>
      </div>
    );
  }
}

Index.propTypes = {
  t: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

export default translate()(Index);
