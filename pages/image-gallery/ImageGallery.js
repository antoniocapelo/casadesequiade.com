import { config } from "config";
import React from "react";
import Lightbox from "react-images";
import { translate } from "react-polyglot";
import { Link } from "react-router";
import { CSSTransitionGroup } from "react-transition-group"; // ES6
// Images
import img4 from "../../assets/images/16.jpg";
import img5 from "../../assets/images/18.jpg";
import img6 from "../../assets/images/25.jpg";
import img1 from "../../assets/images/3.jpg";
import img3 from "../../assets/images/9.jpg";
import Button from "../../shared/components/button/Button";
// Components
import SectionTitle from "../../shared/components/section-title/SectionTitle";
// Styles
import styles from "./ImageGallery.module.css";

// filter types
const exterior = config.filter_exterior;
const house = config.filter_house;
const appartments = config.filter_appartments;
const all = config.filter_all;

class ImageGallery extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      lightboxIsOpen: false,
      currentImage: 0,
      imageFilter: all,
    };

    this.images = props.images || [
      { src: img1, type: exterior },
      { src: img3, type: house },
      { src: img4, type: exterior },
      { src: img5, type: house },
      { src: img6, type: appartments },
    ];

    this.closeLightbox = this.closeLightbox.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);
    this.gotoImage = this.gotoImage.bind(this);
    this.handleClickImage = this.handleClickImage.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
  }

  setFilter(imageFilter) {
    this.setState({ imageFilter });
  }

  render() {
    const { t } = this.props;

    return (
      <div className={styles.imageGallery}>
        <SectionTitle id="gallery">
          {t("home.image-gallery.title")}
        </SectionTitle>

        {this.renderFilter()}
        {this.renderGallery()}

        <Lightbox
          currentImage={this.state.currentImage}
          images={this.filterImages(this.images)}
          isOpen={this.state.lightboxIsOpen}
          onClickImage={this.handleClickImage}
          onClickNext={this.gotoNext}
          onClickPrev={this.gotoPrevious}
          onClickThumbnail={this.gotoImage}
          onClose={this.closeLightbox}
          showThumbnails={this.props.showThumbnails}
          theme={this.props.theme}
        />

        {this.renderButton()}
      </div>
    );
  }

  renderButton() {
    if (!this.props.showLink) {
      return null;
    }
    return (
      <Button className={styles.btn} onClick={this.showContent}>
        <Link to="gallery/" className={styles.btnAnchor}>
          {this.props.t("images.see-full-gallery")}
        </Link>
      </Button>
    );
  }

  filterImages(images) {
    return images.filter((obj) => {
      if (this.state.imageFilter === all) {
        return true;
      }

      return obj.type === this.state.imageFilter;
    });
  }

  openLightbox(index, event) {
    event.preventDefault();

    this.setState({
      currentImage: index,
      lightboxIsOpen: true,
    });
  }

  closeLightbox() {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }

  gotoPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }

  gotoNext() {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }
  gotoImage(index) {
    this.setState({
      currentImage: index,
    });
  }
  renderGallery() {
    const { images } = this;

    if (!images) {
      return;
    }

    const gallery = this.filterImages(images).map((obj, i) => {
      return (
        <a
          href={obj.src}
          className={styles.imageAnchor}
          key={i}
          onClick={(e) => this.openLightbox(i, e)}
        >
          <img src={obj.src} className={styles.thumbnail} />
        </a>
      );
    });

    return (
      <div className={styles.galleryContent}>
        <CSSTransitionGroup
          transitionName={{
            enter: styles.enter,
            enterActive: styles.enterActive,
            leave: styles.leave,
            leaveActive: styles.leaveActive,
          }}
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}
        >
          {gallery}
        </CSSTransitionGroup>
      </div>
    );
  }

  renderFilter() {
    if (!this.props.showFilter) {
      return null;
    }

    const { t } = this.props;

    return (
      <ul className={styles.filters}>
        <li
          onClick={() => this.setFilter(all)}
          className={
            this.state.imageFilter === all
              ? `${styles.filter} ${styles.filterSelected}`
              : styles.filter
          }
        >
          {t("home.image-gallery.filter-all")}
        </li>
        <li
          onClick={() => this.setFilter(exterior)}
          className={
            this.state.imageFilter === exterior
              ? `${styles.filter} ${styles.filterSelected}`
              : styles.filter
          }
        >
          {t("home.image-gallery.filter-exterior")}
        </li>
        <li
          onClick={() => this.setFilter(house)}
          className={
            this.state.imageFilter === house
              ? `${styles.filter} ${styles.filterSelected}`
              : styles.filter
          }
        >
          {t("home.image-gallery.filter-house")}
        </li>
        <li
          onClick={() => this.setFilter(appartments)}
          className={
            this.state.imageFilter === appartments
              ? `${styles.filter} ${styles.filterSelected}`
              : styles.filter
          }
        >
          {t("home.image-gallery.filter-appartments")}
        </li>
      </ul>
    );
  }

  handleClickImage() {
    if (this.state.currentImage === this.filterImages(this.images).length - 1) {
      this.closeLightbox();
    }

    this.gotoNext();
  }
}

ImageGallery.propTypes = {
  images: React.PropTypes.array,
  showFilter: React.PropTypes.bool,
  showLink: React.PropTypes.bool,
  theme: React.PropTypes.string,
  showThumbnails: React.PropTypes.bool,
  t: React.PropTypes.func.isRequired,
};

export default translate()(ImageGallery);
