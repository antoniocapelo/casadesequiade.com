import React from 'react';
import GoogleMapReact from 'google-map-react';
import logo from '../../assets/sequiade-simple-logo.png';
import {translate} from 'react-polyglot';

// Components
import SectionTitle from '../../shared/components/section-title/SectionTitle';

// GoogleMaps page
const url =
  'https://www.google.pt/maps/place/Casa+de+Sequiade/@41.5019298,-8.541921,17.21z/data=!4m13!1m7!3m6!1s0x0:0x0!2zNDHCsDMwJzA3LjAiTiA4wrAzMicyNC4zIlc!3b1!8m2!3d41.501949!4d-8.54008!3m4!1s0x0:0x501d37ec4f037699!8m2!3d41.5020978!4d-8.5405337';

// Styles
import styles from './Location.module.css';

const mapData = {
  center: {lat: 41.501646, lng: -8.542286},
  pin: {lat: 41.501872, lng: -8.540551},
  zoom: 17,
};

class Pin extends React.Component {
  render() {
    return (
      <a href={url} target="_blank" className={styles.anchor}>
        <img src={logo} className={styles.pin} />
        <h6>Casa de Sequiade</h6>
      </a>
    );
  }
}

class Location extends React.Component {
  render() {
    const {t} = this.props;

    return (
      <div className={styles.location}>
        <SectionTitle id="location">{t('home.location.title')}</SectionTitle>
        <div className={styles.map}>
          <GoogleMapReact
            bootstrapURLKeys={{key: 'AIzaSyDZ6BuGwwME79l9FHMCboAY8k2QA-BrEm8'}}
            defaultCenter={mapData.pin}
            defaultZoom={mapData.zoom}>
            <Pin lat={mapData.pin.lat} lng={mapData.pin.lng} />
          </GoogleMapReact>
        </div>
        {this.renderDetails()}
      </div>
    );
  }

  renderDetails() {
    const {t} = this.props;

    return (
      <div className={styles.locationDetails}>
        <div className={styles.locationDetail}>
          <h4 className={styles.locationDetailsTitle}>
            {t('home.location.gps')}
          </h4>
          <div className={styles.gpsLocation}>
            <p className={styles.locationDetailInfo}>
              {'N 41 ° 30\' 06.50" '}{' '}
            </p>
            <p className={styles.locationDetailInfo}> {'W 08 ° 32\' 27.5"'}</p>
          </div>
        </div>
        <div className={styles.locationDetail}>
          <h4 className={styles.locationDetailsTitle}>
            {t('home.location.address')}
          </h4>
          <p className={styles.locationDetailInfo}>
            Rua da Piedade, 61 <br /> 4755-508 Sequeade
          </p>
        </div>
      </div>
    );
  }
}

Location.propTypes = {
  t: React.PropTypes.func.isRequired,
};

export default translate()(Location);
