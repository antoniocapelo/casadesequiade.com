import React from 'react';
import Helmet from 'react-helmet';
import { config } from 'config';
import { translate } from 'react-polyglot';
import { Link } from 'react-router';

// Components
import Button from '../shared/components/button/Button';

// Styles
import styles from './index.module.css';

class NotFound extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showContent: false,
        };
    }

    componentDidMount() {
        window.scroll(0, 0);
        setTimeout(() => {
            this.setState({ showContent: true });
            if (this.props.location.hash) {
                const element = document.querySelector(this.props.location.hash);

                if (element) { element.scrollIntoView({ behavior: 'smooth' }); }
            }
        }, 750);
    }

    render() {
        const { t } = this.props;

        return (
            <div className={ styles.homeWrapper }>
                <div className={ styles.home }>
                    <Helmet
                        title={ config.siteTitle }
                        meta={ [
                    { name: 'description', content: t('main.description') },
                    { name: 'keywords', content: t('main.keywords') },
                        ] }/>
                    <h1 className={ styles.homeTitle }>
                        Casa de Sequiade
                    </h1>
                    <h3 className={ styles.homeSubtitle }>
                        ~ Turismo Rural ~
                    </h3>
                    <h3 className={ styles.homeSubtitle }>
                        { t('main.page-not-found') }
                    </h3>
                    <Button className={ styles.btn } onClick={ this.showContent }>
                        <Link to="/" className={ styles.btnAnchor }>
                            { this.props.t('main.go-to-home') }
                        </Link>
                    </Button>
                </div>
            </div>
        );
    }
}

NotFound.propTypes = {
    t: React.PropTypes.func.isRequired,
    location: React.PropTypes.object.isRequired,
};

export default translate()(NotFound);
