import { config } from "config";
import React from "react";
import Helmet from "react-helmet";
import { translate } from "react-polyglot";
// Images
import img1 from "../assets/images/1.jpg";
import img10 from "../assets/images/10.jpg";
import img11 from "../assets/images/11.jpg";
import img12 from "../assets/images/12.jpg";
import img13 from "../assets/images/13.jpg";
import img14 from "../assets/images/14.jpg";
import img15 from "../assets/images/15.jpg";
import img16 from "../assets/images/16.jpg";
import img17 from "../assets/images/17.jpg";
import img18 from "../assets/images/18.jpg";
import img19 from "../assets/images/19.jpg";
import img2 from "../assets/images/2.jpg";
import img20 from "../assets/images/20.jpg";
import img21 from "../assets/images/21.jpg";
import img22 from "../assets/images/22.jpg";
import img23 from "../assets/images/23.jpg";
import img24 from "../assets/images/24.jpg";
import img25 from "../assets/images/25.jpg";
import img26 from "../assets/images/26.jpg";
import img27 from "../assets/images/27.jpg";
import img29 from "../assets/images/29.jpg";
import img3 from "../assets/images/3.jpg";
import img30 from "../assets/images/30.jpg";
import img31 from "../assets/images/31.jpg";
import img32 from "../assets/images/32.jpg";
import img33 from "../assets/images/33.jpg";
import img34 from "../assets/images/34.jpg";
import img35 from "../assets/images/35.jpg";
import img36 from "../assets/images/36.jpg";
import img37 from "../assets/images/37.jpg";
import img38 from "../assets/images/38.jpg";
import img39 from "../assets/images/39.jpg";
import img4 from "../assets/images/4.jpg";
import img40 from "../assets/images/40.jpg";
import img41 from "../assets/images/41.jpg";
import img42 from "../assets/images/42.jpg";
import img43 from "../assets/images/43.jpg";
import img44 from "../assets/images/44.jpg";
import img45 from "../assets/images/45.jpg";
import img46 from "../assets/images/46.jpg";
import img47 from "../assets/images/47.jpg";
import img5 from "../assets/images/5.jpg";
import img6 from "../assets/images/6.jpg";
import img7 from "../assets/images/7.jpg";
import img8 from "../assets/images/8.jpg";
import img9 from "../assets/images/9.jpg";
import kitchen from "../assets/images/kitchen.jpg";
// Styles
import styles from "./gallery.module.css";
// Components
import ImageGallery from "./image-gallery/ImageGallery";

// Full image set
const images = [
  { src: img1, type: config.filter_exterior },
  { src: img2, type: config.filter_exterior },
  { src: img3, type: config.filter_exterior },
  { src: img4, type: config.filter_exterior },
  { src: img5, type: config.filter_exterior },
  { src: img6, type: config.filter_exterior },
  { src: img7, type: config.filter_exterior },
  { src: img8, type: config.filter_exterior },
  { src: img9, type: config.filter_exterior },
  { src: img10, type: config.filter },
  { src: img11, type: config.filter_exterior },
  { src: img12, type: config.filter_exterior },
  { src: img13, type: config.filter },
  { src: img14, type: config.filter },
  { src: img15, type: config.filter },
  { src: img16, type: config.filter_exterior },
  { src: img17, type: config.filter_appartments },
  { src: img18, type: config.filter_exterior },
  { src: img19, type: config.filter_exterior },
  { src: img20, type: config.filter_exterior },
  { src: img21, type: config.filter_exterior },
  { src: img22, type: config.filter_exterior },

  { src: img23, type: config.filter_house },
  { src: img24, type: config.filter_house },
  { src: img25, type: config.filter_house },
  { src: img26, type: config.filter_house },
  { src: img27, type: config.filter_house },
  { src: img29, type: config.filter_house },
  { src: img30, type: config.filter_house },
  { src: img31, type: config.filter_house },
  { src: img32, type: config.filter_house },
  { src: img33, type: config.filter_house },
  { src: img34, type: config.filter_house },
  { src: kitchen, type: config.filter_house },

  { src: img35, type: config.filter_appartments },
  { src: img36, type: config.filter_appartments },
  { src: img37, type: config.filter_appartments },
  { src: img38, type: config.filter_appartments },
  { src: img39, type: config.filter_appartments },
  { src: img40, type: config.filter_appartments },
  { src: img41, type: config.filter_appartments },
  { src: img42, type: config.filter_appartments },
  { src: img43, type: config.filter_appartments },
  { src: img44, type: config.filter_appartments },
  { src: img45, type: config.filter_appartments },

  { src: img46, type: config.filter_exterior },
  { src: img47, type: "" },
];

class Gallery extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showContent: false,
    };
  }

  componentDidMount() {}

  render() {
    const { t } = this.props;

    return (
      <div className={styles.galleryWrapper}>
        <div className={styles.gallery}>
          <Helmet
            title={config.siteTitle}
            meta={[
              { name: "description", content: t("main.description") },
              { name: "keywords", content: t("main.keywords") },
            ]}
          />
          <h1 className={styles.homeTitle}>Casa de Sequiade</h1>
          <ImageGallery images={images} showFilter />
        </div>
      </div>
    );
  }
}

Gallery.propTypes = {
  t: React.PropTypes.func.isRequired,
};

export default translate()(Gallery);
