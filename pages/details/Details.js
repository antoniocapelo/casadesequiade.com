import React from 'react';
import Modal from 'react-modal';
import { translate } from 'react-polyglot';

// Components
import Button from '../../shared/components/button/Button';

// Styles
import styles from './Details.module.css';

// Images
import logo from '../../assets/logo.png';

class Details extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showDetails: false,
        };

        this.showContent = this.showContent.bind(this);
        this.hideDetails = this.hideDetails.bind(this);
    }

    render() {
        const btnClass = [styles.btn];

        return (
            <div className={ styles.details } >
                <Button className={ btnClass.join(', ') } onClick={ this.showContent }>
                    { this.props.t('details.know-more') }
                </Button>
                { this.renderDetails() }
            </div>
        );
    }

    renderDetails() {
        if (this.context.locale === 'pt') {
            return (
                <Modal
                    onRequestClose={ this.hideDetails }
                    className={ styles.modal }
                    overlayClassName={ styles.overlay }
                    isOpen={ this.state.showDetails }
                    closeTimeoutMS={ 100 }
                    contentLabel="Modal">
                    <img src={ logo } className={ styles.detailsModalLogo } />
                    <p className={ styles.detail }>Com cerca de 250 anos de idade, esta casa foi reconstruída e remodelada para proporcionar um turismo de habitação de referência, com qualidade e simpatia.</p>
                    <p className={ styles.detail }>Foram mantidas as estruturas e os materiais tão típicos destas construções minhotas, preservando no granito a beleza e solidez que saltam à vista de quem aqui entra. </p>
                    <p className={ styles.detail }>Os quartos da casa, situados num piso independente, estão inseridos num meio mais familiar, pertencem ao edifício original e têm uma sala de estar com lareira, sendo extremamente confortáveis.</p>
                    <p className={ styles.detail }>Já os apartamentos, são mais amplos e integram uma sala de estar com pequena cozinha, no piso inferior, e um quarto com varanda, no piso superior</p>
                    <p className={ styles.detail }>A casa dispõe ainda de um "salão de jogos", onde é possível descontrair com uma partida de snooker, dardos, ouvir música, ou assistir TV.</p>
                    <p className={ styles.detail }>Os espaços exteriores da Casa de Sequiade são decorados com relva e flores que envolvem a pérgola, a piscina e a cama de jardim, onde se pode descansar, ler um livro, ou simplesmente desfrutar da calma do campo.</p>
                    <p className={ styles.detail }>Aguardamos a sua visita, com simpatia e hospitalidade.</p>
                </Modal>
            );
        }
        return (
            <Modal
                onRequestClose={ this.hideDetails }
                className={ styles.modal }
                overlayClassName={ styles.overlay }
                isOpen={ this.state.showDetails }
                closeTimeoutMS={ 100 }
                contentLabel="Modal">
                <img src={ logo } className={ styles.detailsModalLogo } />
            <p>
                With about 250 years old, the house was rebuilt and remodeled to become a reference for tourism in the countryside, based on quality and friendliness.
            </p>
            <p>
            The structures and materials, so typical of these small buildings, were preserved, showing the granite and the natural beauty for anyone who enters the house.
            </p>
            <p>
                The rooms of the house, situated on a separate floor, are inserted in a more familiar environment, belonged to the original building and have a living room with fireplace, being extremely comfortable.
            </p>
            <p>
                The apartments are larger and include a living room with small kitchen on the lower floor and a bedroom with balcony on the upper floor.
            </p>
            <p>
            The house also has a "gaming room" where you can relax with a game of snooker, darts, listen to music, or watch TV.
            </p>
            <p>
            The exterior spaces of Casa de Sequiade are decorated with grass and flowers that surround the pergola, the swimming pool and the garden bed, where you can rest, read a book, or simply enjoy the calm of the countryside.
            </p>
            <p>
                We look forward to your visit, with kindness and hospitality.
            </p>
            </Modal>
        );
    }

    hideDetails() {
        this.setState({ showDetails: false });
    }

    showContent() {
        this.setState({ showDetails: true });
    }
}

Details.contextTypes = {
    locale: React.PropTypes.string,
};

Details.propTypes = {
    t: React.PropTypes.func.isRequired,
};

export default translate()(Details);
