import React from "react";
import FaEnvelope from "react-icons/lib/fa/envelope";
import FaFacebookOfficial from "react-icons/lib/fa/facebook-official";
import FaInstagram from "react-icons/lib/fa/instagram";
// Icons
import FaMobile from "react-icons/lib/fa/mobile";
import { translate } from "react-polyglot";
// Components
import SectionTitle from "../../shared/components/section-title/SectionTitle";
// Styles
import styles from "./Contacts.module.css";

const contacts = [
  {
    type: "phone",
    href: "+351919519865",
    text: "00 351  919 519 865",
    component: FaMobile,
  },
  {
    type: "phone",
    href: "+351917522730",
    text: "00 351  917 522 730",
    component: FaMobile,
  },
  {
    type: "email",
    href: " geral@casadesequiade.com",
    text: " geral@casadesequiade.com",
    component: FaEnvelope,
  },
];

class Contacts extends React.Component {
  constructor(props) {
    super(props);

    this.renderContact = this.renderContact.bind(this);
  }

  render() {
    const { t } = this.props;

    return (
      <div className={styles.contacts}>
        <SectionTitle id="contacts">{t("home.contacts.title")}</SectionTitle>
        <ul className={styles.contactList}>
          {contacts.map(this.renderContact)}
        </ul>
        <div className={styles.social}>
          {t("home.contacts.follow-us")}
          <ul className={styles.socialList}>
            <li className={styles.socialItem}>
              <a
                href="https://www.instagram.com/casadesequiade/"
                target="_blank"
                className={styles.anchor}
              >
                <span className={styles.contactType}>
                  <FaInstagram className={styles.socialIcon} />
                </span>
              </a>
            </li>
            <li className={styles.socialItem}>
              <a
                href="https://www.facebook.com/casadesequiade/"
                target="_blank"
                className={styles.anchor}
              >
                <span className={styles.contactType}>
                  <FaFacebookOfficial className={styles.socialIcon} />
                </span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    );
  }

  renderContact(contact) {
    const Comp = contact.component;

    return (
      <li className={styles.contactListItem} key={contact.href}>
        <a
          href={
            contact.type === "phone"
              ? `tel:${contact.href}`
              : `mailto:${contact.href}`
          }
          className={styles.anchor}
        >
          <span className={styles.contactType}>
            <Comp className={styles.contactIcon} />
          </span>
          {contact.text}
        </a>
      </li>
    );
  }
}

Contacts.propTypes = {
  t: React.PropTypes.func.isRequired,
};

export default translate()(Contacts);
