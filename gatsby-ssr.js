import React from 'react';
import { I18n } from 'react-polyglot';

const locale = 'pt';
const messages = require(`./i18n/${locale}.js`);

// This gets returned to Gatsby which then renders the Root component as normal.
exports.wrapRootComponent = (Root) => {
    return () => (
        <I18nWrapper locale={ locale } messages={ messages }>
            <Root />
        </I18nWrapper>
    );
};

class I18nWrapper extends React.Component {
    getChildContext() {
        return { locale: this.props.locale };
    }

    render() {
        return (
            <I18n locale={ this.props.locale } messages={ this.props.messages }>
                { this.props.children }
            </I18n>
        );
    }
}

I18nWrapper.childContextTypes = {
    locale: React.PropTypes.string,
};

I18nWrapper.propTypes = {
    locale: React.PropTypes.string,
    messages: React.PropTypes.object,
    children: React.PropTypes.element,
};
