import React, { Component } from "react";
import { translate } from "react-polyglot";
import livro from "../../../assets/images/livro.png";
import styles from "./Footer.module.css";

class Footer extends Component {
  render() {
    const { t, ...rest } = this.props;

    const isPt = t("language") === "pt";
    return (
      <footer className={styles.footer}>
        <div className={styles.footerContent}>
          <div className={styles.links}>
            <a
              rel="noreferrer noopener"
              target="_blank"
              href={
                isPt
                  ? "/documents/Politica-Geral-de-Seguranca-e-Privacidade.pdf"
                  : "/documents/General-Security-and-Privacy-Policy.pdf"
              }
            >
              {t("footer.privacy-policy")}
            </a>
            <a
              rel="noreferrer noopener"
              target="_blank"
              href={"/documents/Casa_de_Sequiade-Plano_de_contingencia.pdf"}
            >
              {t("footer.contingency-plan")}
            </a>
            <div className={styles.rnet}>
              <a
                className={styles.anchor}
                href="https://registos.turismodeportugal.pt/RNET.aspx?nr=516&close=True"
                title="Nº de Registo Nacional de Empreendimentos Turísticos"
                target="_blank"
                rel="noreferrer noopener"
              >
                RNET
              </a>{" "}
              - 516
            </div>

            <a
              className={styles.livro}
              title="Livro de reclamações online"
              href="https://www.livroreclamacoes.pt/inicio"
              target="_blank"
              rel="noreferrer noopener"
            >
              <img src={livro} />
            </a>
          </div>
          <div className={styles.copyright}>
            <span>Casa de Sequiade - 2021</span>
          </div>
        </div>
      </footer>
    );
  }
}

export default translate()(Footer);
