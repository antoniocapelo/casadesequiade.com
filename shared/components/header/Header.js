import React, { Component } from 'react';
import { Container } from 'react-responsive-grid';
import { Link } from 'react-router';
import { translate } from 'react-polyglot';
import styles from './Header.module.css';

// Icons
import FaBars from 'react-icons/lib/fa/bars';

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showMenu: false,
        };

        this.scrollToSection = this.scrollToSection.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.hideMenu = this.hideMenu.bind(this);
        this.chooseLanguage = this.chooseLanguage.bind(this);
    }

    componentDidMount() {
        this.className = 'menu-open';
        this.context.router.listen(() => this.hideMenu());
        this.el = document.body;
    }

    render() {
        const { t } = this.props;

        return (
            <div className={ styles.headerWrapper } >
                <Container
                    className={ styles.header }
                    style={ {
                        maxWidth: 'none',
                        padding: 10,
                    } }>
                    { this.renderLanguageSwitcher() }
                    <a onClick={ this.toggleMenu } className={ styles.menuIcon } ><FaBars /></a>

                    <Link to="/" className={ styles.navItem }>{ t('header.home') }</Link>
                    <Link to="/gallery/" className={ styles.navItem }>{ t('header.gallery') }</Link>
                    <a onClick={ this.scrollToSection.bind(this, 'location') } className={ styles.navItem }>{ t('header.location') }</a>
                    <a onClick={ this.scrollToSection.bind(this, 'contacts') } className={ styles.navItem }>{ t('header.contacts') }</a>
                    { this.renderMobileMenu() }
                </Container>
                { this.state.showMenu && <div onClick={ this.hideMenu } className={ styles.overlay } />}
            </div>
        );
    }

    renderLanguageSwitcher() {
        const lang = this.context.locale;
        const ptClass = [styles.languageAnchor].concat(lang === 'pt' ? [styles.selectedLanguageAnchor] : []).join(' ');
        const enClass = [styles.languageAnchor].concat(lang === 'en' ? [styles.selectedLanguageAnchor] : []).join(' ');

        return (
            <ul className={ styles.languageSwitcher }>
                <li className={ styles.language }>
                    <a className={ ptClass } href="" onClick={ this.chooseLanguage.bind(this, 'pt') }>PT</a>
                </li>
                <li className={ styles.language }>
                    <a className={ enClass } href="" onClick={ this.chooseLanguage.bind(this, 'en') }>EN</a>
                </li>
            </ul>
        );
    }

    chooseLanguage(language, ev) {
        ev.preventDefault();

        if (window.localStorage.getItem('locale') !== language) {
            window.localStorage.setItem('locale', language);
            window.location.reload();
        }
    }

    renderMobileMenu() {
        const classNames = [styles.mobileMenu];
        const { t } = this.props;

        if (this.state.showMenu) {
            classNames.push(styles.mobileMenuActive);
        }

        return (
            <div className={ classNames.join(' ') } >
                <a onClick={ this.toggleMenu } className={ styles.menuIconActive } ><FaBars /></a>
                <Link to="/" className={ styles.mobileNavItem }>{ t('header.home') }</Link>
                <Link to="gallery/" className={ styles.mobileNavItem }>{ t('header.gallery') }</Link>
                <a onClick={ this.scrollToSection.bind(this, 'location') } className={ styles.mobileNavItem } >{ t('header.location') }</a>
                <a onClick={ this.scrollToSection.bind(this, 'contacts') } className={ styles.mobileNavItem } >{ t('header.contacts') }</a>
            </div>
        );
    }

    scrollToSection(sectionId, ev) {
        const { path } = this.props;

        ev.preventDefault();

        if (path === '/') {
            this.setState({ showMenu: false });
            const element = document.getElementById(sectionId);

            this.hideMenu();

            if (element) { window.scrollTo(0, element.offsetTop - 50, { behavior: 'smooth' }); }
        } else {
            this.context.router.push(`/#${sectionId}`);
        }
    }

    toggleMenu() {
        this.toggleBodyClass();

        this.setState({
            showMenu: !this.state.showMenu,
        });
    }

    hideMenu() {
        this.removeBodyClass();
        this.setState({
            showMenu: false,
        });
    }

    toggleBodyClass() {
        if (!this.el) {
            return;
        }

        const el = this.el;
        const className = this.className;


        if (el.classList) {
            el.classList.toggle(className);
        } else {
            const classes = el.className.split(' ');
            const existingIndex = classes.indexOf(className);

            if (existingIndex >= 0) { classes.splice(existingIndex, 1); } else { classes.push(className); }

            el.className = classes.join(' ');
        }
    }

    removeBodyClass() {
        if (!this.el) {
            return;
        }

        const el = this.el;
        const className = this.className;

        if (el.classList) {
            el.classList.remove(className);
        } else {
            el.className = el.className.replace(new RegExp(`(^|\\b)${className.split(' ').join('|')}(\\b|$)`, 'gi'), ' ');
        }
    }
}

Header.contextTypes = {
    locale: React.PropTypes.string,
    router: React.PropTypes.object.isRequired,
};

Header.propTypes = {
    t: React.PropTypes.func.isRequired,
    path: React.PropTypes.string.isRequired,
    history: React.PropTypes.object.isRequired,
};

export default translate()(Header);
