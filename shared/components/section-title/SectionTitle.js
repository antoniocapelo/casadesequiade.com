import React, { Component } from 'react';
import styles from './SectionTitle.module.css';

class SectionTitle extends Component {
    render() {
        return (
            <h2 className={ styles.title } { ...this.props }>
                { this.props.children }
            </h2>
        );
    }
}

SectionTitle.propTypes = {
    children: React.PropTypes.string,
};


export default SectionTitle;

