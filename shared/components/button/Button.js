import React, { PropTypes, Component } from 'react';
import styles from './Button.module.css';


class Button extends Component {

    render() {
        const { children, className, ...remainingProps } = this.props;
        const classes = [styles.button];

        if (className) {
            classes.push(className);
        }
        return (
            <button className={ classes.join(' ') } type="button" { ...remainingProps } >
                { children }
            </button>
        );
    }
}

Button.propTypes = {
    children: PropTypes.any,
    className: PropTypes.string,
};

export default Button;
